﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Music_Player.Startup))]
namespace Music_Player
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
