﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Music_Player.Models;

namespace Music_Player.Controllers
{
    public class HomeController : Controller
    {
        
        private PlayerContext db = new PlayerContext();

        // GET: Songs
        public ActionResult Index()
        {
            var songs = db.Songs.Include(s => s.Album.Artist);
            return View(songs.ToList());
        }
    }
}