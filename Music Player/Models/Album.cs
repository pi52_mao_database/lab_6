﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Music_Player.Models
{
    public class Album
    {
        public int Id { get; set; }
        [Display(Name = "Album")]
        public string Name { get; set; }
        public int Releasedate { get; set; }
        public int ArtistId { get; set; }
        public Artist Artist { get; set; }

        public ICollection<Song> Songs { get; set; }
        public Album()
        {
            Songs = new List<Song>();
        }

    }
}

