﻿using System.Data.Entity;

namespace Music_Player.Models
{
    
    public class PlayerContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<PlayerContext>(null);
            base.OnModelCreating(modelBuilder);
        }
        public PlayerContext() : base("DefaultConnection") { }
        public DbSet<Song> Songs { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
    
}