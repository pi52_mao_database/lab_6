﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Music_Player.Models
{
    public class Artist
    {
        public int Id { get; set; }
        [Display(Name = "Artist")]
        public string Name { get; set; }
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
        public ICollection<Album> Albums { get; set; }
        public Artist()
        {
            Albums = new List<Album>();
        }
    }
}