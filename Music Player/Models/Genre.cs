﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Music_Player.Models
{
    public class Genre
    {
        public int Id { get; set; }
        [Display(Name = "Genre")]
        public string Name { get; set; }
        public ICollection<Artist> Artists { get; set; }
        public Genre()
        {
            Artists = new List<Artist>();
        }
    }
}